from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=200)
    id = models.PositiveIntegerField(primary_key=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})

    def __str__(self):
        return self.name


class Appointment(models.Model):
    owner = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    reason = models.TextField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    vin = models.CharField(max_length=17)
    canceled = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.PROTECT
    )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return f"appointment for {self.owner}, VIP {self.vip}"
