from django.db import models
from django.core.validators import RegexValidator

# Create your models here.


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number_regex = RegexValidator(
        regex=r"^[0-9]+(\.[0-9]+)?$",
        message="Can't be a negative number.",
    )
    employee_number = models.PositiveBigIntegerField(validators=[employee_number_regex])

    def __str__(self):
        return self.name


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    phone_regex = RegexValidator(
        regex=r"^\(\d{3}\) \d{3}-\d{4}$",
        message="Phone number must be entered as: '(000) 000-0000'.",
    )
    phone_number = models.CharField(
        validators=[phone_regex],
        max_length=17,
        blank=True,
    )

    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)

    def __str__(self):
        return self.vin


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="customer",
        on_delete=models.PROTECT,
    )
    sale_price_regex = RegexValidator(
        regex=r"^[0-9]+(\.[0-9]+)?$",
        message="Can't be a negative number.",
    )
    sale_price = models.PositiveBigIntegerField(
        validators=[sale_price_regex],
        default=0,
    )

    def __str__(self):
        return f"Sale by {self.sales_person.name} of {self.automobile.vin} to {self.customer.name} for ${self.sale_price}"
