from django.urls import path
from sales_rest.views import (
    list_sales_people,
    show_sales_person,
    list_potential_cutomers,
    show_potential_cutomer,
    list_sales,
    show_sale,
)


urlpatterns = [
    path(
        "sales_people/",
        list_sales_people,
        name="list_sales_people",
    ),
    path(
        "sales_people/<int:id>",
        show_sales_person,
        name="show_sales_person",
    ),
    path(
        "potential_cutomers/",
        list_potential_cutomers,
        name="list_potential_cutomers",
    ),
    path(
        "potential_cutomers/<int:id>",
        show_potential_cutomer,
        name="show_potential_cutomer",
    ),
    path(
        "sales/",
        list_sales,
        name="list_sales",
    ),
    path(
        "sales/<int:id>",
        show_sale,
        name="show_sale",
    ),
]
