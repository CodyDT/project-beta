from django.contrib import admin
from sales_rest.models import (
    SalesPerson,
    PotentialCustomer,
    Sale,
    AutomobileVO,
)

# Register your models here.


@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "employee_number",
    )


@admin.register(PotentialCustomer)
class PotentialCustomerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "address",
        "phone_number",
    )


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        "automobile",
        "sales_person",
        "customer",
        "sale_price",
    )


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = (
        "import_href",
        "vin",
    )
