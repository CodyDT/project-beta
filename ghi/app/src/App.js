import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import NewTechnicianForm from "./NewTechnicianForm";
import AppointmentForm from "./NewAppointment";
import AppointmentList from "./AppointmentList";
import ServiceHistory from "./ServiceHistory";

import SalesPeopleList from "./SalesPeopleList";
import SalesPersonForm from "./SalesPersonForm";
import ModelForm from "./ModelForm";
import ModelList from "./ModelList";
import AutoForm from "./AutoForm";
import AutoList from "./AutoList";
import Manufacturers from "./ManufacturerList";
import NewManufacturer from "./NewManufacturer";
import PotentialCutomersList from "./PotentialCustomersList";
import PotentialCustomerForm from "./PotentialCustomerForm";
import SaleForm from "./SaleForm";
import SaleList from "./SaleList";
import SalesPersonHistory from "./SalesPersonHistory";

function App(props) {
  return (
    <BrowserRouter basename="/">
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales_people">
            <Route
              index
              element={<SalesPeopleList salesPeople={props.salesPeople} />}
            />
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="potential_customers">
            <Route
              index
              element={
                <PotentialCutomersList
                  potentialCutomers={props.potentialCutomers}
                />
              }
            />
            <Route path="new" element={<PotentialCustomerForm />} />
          </Route>
          <Route path="autos">
            <Route index element={<AutoList autos={props.autos} />} />
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList models={props.models} />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="manufacturers">
            <Route
              path=""
              element={<Manufacturers manufacturers={props.manufacturers} />}
            />
            <Route path="new" element={<NewManufacturer />} />
          </Route>
          <Route path="sales">
            <Route index element={<SaleList sales={props.sales} />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="history">
            <Route index element={<SalesPersonHistory />} />
          </Route>
          <Route path="/technician">
            <Route path="new" element={<NewTechnicianForm />} />
          </Route>
          <Route path="/appointment/">
            <Route path="/appointment/" element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route>
            <Route path="/appointment/history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
