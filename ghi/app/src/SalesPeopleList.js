import React from "react";

class SalesPeopleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salesPeople: [],
    };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/sales/sales_people/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ salesPeople: data.sales_people });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {this.state.salesPeople.map((salesPerson) => {
            return (
              <tr key={salesPerson.employee_number}>
                <td>{salesPerson.name}</td>
                <td>{salesPerson.employee_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default SalesPeopleList;
