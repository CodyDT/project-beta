import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/autos">
                Automobiles
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/autos/new">
                New Automobile
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">
                Models
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/models/new"
              >
                New Vehicle Model
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">
                Manufacturers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/manufacturers/new"
              >
                New Manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales_people">
                Sales People
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/sales_people/new"
              >
                New Sales Person
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/potential_customers">
                Potential Customers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/potential_customers/new"
              >
                New Potential Customer
              </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales">
                Sales
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/sales/new">
                New Sale
              </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/history">
                Sales History
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/appointment/"
              >
                Appointments
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/appointment/history"
              >
                Appointment History
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/technician/new"
              >
                New Tech
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link"
                aria-current="page"
                to="/appointment/new"
              >
                New Appointment
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
