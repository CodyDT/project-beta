import React from "react";

class SaleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales: [],
    };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/sales/sales/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales: data.sales });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Employee Number</th>
            <th>Purchaser's Name</th>
            <th>Automobile VIN</th>
            <th>Price of Sale</th>
          </tr>
        </thead>
        <tbody>
          {this.state.sales.map((sale) => {
            return (
              <tr key={sale.automobile.vin}>
                <td>{sale.sales_person.name}</td>
                <td>{sale.sales_person.employee_number}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>
                  {sale.sale_price.toLocaleString("en-US", {
                    style: "currency",
                    currency: "USD",
                  })}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default SaleList;
