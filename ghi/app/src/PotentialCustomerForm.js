import React from "react";

class PotentialCustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      phone_number: "",
    };

    this.handleChange = (event) => {
      const newState = {};
      newState[event.target.id] = event.target.value;
      this.setState(newState);
    };

    this.handleSubmit = async (event) => {
      event.preventDefault();
      const data = { ...this.state };

      const POTENTIAL_CUSTOMERS_URL =
        "http://localhost:8090/sales/potential_cutomers/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(POTENTIAL_CUSTOMERS_URL, fetchConfig);
      if (response.ok) {
        this.setState({
          name: "",
          address: "",
          phone_number: "",
        });
      } else {
        console.error(response);
      }
    };
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new potential customer</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="mname">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChange}
                  value={this.state.address}
                  placeholder="Address"
                  required
                  type="text"
                  name="address"
                  id="address"
                  className="form-control"
                />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChange}
                  value={this.state.phone_number}
                  placeholder="Phone number"
                  required
                  type="text"
                  name="phone_number"
                  id="phone_number"
                  className="form-control"
                />
                <label htmlFor="phone_number">Phone number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PotentialCustomerForm;
