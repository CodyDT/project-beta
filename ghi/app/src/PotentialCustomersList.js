import React from "react";

class PotentialCustomersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      potentialCustomers: [],
    };
  }

  async componentDidMount() {
    const response = await fetch(
      "http://localhost:8090/sales/potential_cutomers/"
    );
    if (response.ok) {
      const data = await response.json();
      this.setState({ potentialCustomers: data.potential_customers });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {this.state.potentialCustomers.map((potentialCustomer) => {
            return (
              <tr key={potentialCustomer.phone_number}>
                <td>{potentialCustomer.name}</td>
                <td>{potentialCustomer.address}</td>
                <td>{potentialCustomer.phone_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default PotentialCustomersList;
