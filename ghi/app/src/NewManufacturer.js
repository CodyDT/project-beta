import React from "react";

class NewManufacturer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    };
    this.handleChange = (event) => {
      const newState = {};
      newState[event.target.id] = event.target.value;
      this.setState(newState);
    };

    this.handleSubmit = async (event) => {
      event.preventDefault();
      const data = { ...this.state };
      const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(manufacturerUrl, fetchConfig);
      if (response.ok) {
        const cleared = {
          name: "",
        };
        this.setState(cleared);
      } else {
        console.error(response);
      }
    };
  }

  render() {
    return (
      <div>
        <h1>Create a new car manufacturer</h1>
        <form onSubmit={this.handleSubmit} id="">
          <div className="form-floating mb-3">
            <input
              onChange={this.handleChange}
              placeholder="Name"
              required
              type="text"
              name="name"
              id="name"
              className="form-control"
              value={this.state.name}
            />
            <label htmlFor="name">Name</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    );
  }
}

export default NewManufacturer;
