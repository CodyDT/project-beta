import React from "react";

class SalesPersonHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_people: [],
      sales_person: "",
      sales: [],
    };
    this.handleChange = (event) => {
      const newState = {};
      newState[event.target.id] = event.target.value;
      this.setState(newState);
    };
  }

  async componentDidMount() {
    const SALES_PEOPLE_URL = "http://localhost:8090/sales/sales_people/";
    const salesPeopleResponse = await fetch(SALES_PEOPLE_URL);
    if (salesPeopleResponse.ok) {
      const data = await salesPeopleResponse.json();
      this.setState({ sales_people: data.sales_people });
    } else {
      console.error(salesPeopleResponse);
    }

    const SALES_URL = "http://localhost:8090/sales/sales/";
    const salesResponse = await fetch(SALES_URL);
    if (salesResponse.ok) {
      const data = await salesResponse.json();
      this.setState({ sales: data.sales });
    } else {
      console.error(salesResponse);
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.state.sales_person !== prevState.sales_person) {
      const SALES_URL = "http://localhost:8090/sales/sales/";
      const response = await fetch(SALES_URL);
      if (response.ok) {
        const data = await response.json();
        this.setState({ sales: data.sales });
        this.setState({
          sales: data.sales.filter(
            (sale) => sale.sales_person.name === this.state.sales_person
          ),
        });
      } else {
        console.error(response);
      }
    }
  }

  render() {
    return (
      <div>
        <h1>Sales person history</h1>
        <div className="mb-3">
          <select
            onChange={this.handleChange}
            value={this.state.sales_person}
            required
            name="sales_person"
            id="sales_person"
            className="form-select"
          >
            <option value="">Choose a sales person</option>
            {this.state.sales_people.map((sales_person) => {
              return (
                <option key={sales_person.id} value={sales_person.name}>
                  {sales_person.name}
                </option>
              );
            })}
          </select>
        </div>
        {this.state.sales_person && (
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Purchaser's Name</th>
                <th>Automobile VIN</th>
                <th>Price of Sale</th>
              </tr>
            </thead>
            <tbody>
              {this.state.sales.map((sale) => {
                return (
                  <tr key={sale.automobile.vin}>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.sales_person.employee_number}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>
                      {sale.sale_price.toLocaleString("en-US", {
                        style: "currency",
                        currency: "USD",
                      })}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

export default SalesPersonHistory;
